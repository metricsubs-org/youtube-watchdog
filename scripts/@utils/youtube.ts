import * as ChildProcess from 'child_process';
import * as Path from 'path';

import Axios, {AxiosError} from 'axios';
import humanize from 'human-format';

import {YOUTUBE_API_KEY} from './env';

const EXEC_PROGRAM = 'yt-dlp';
const YOUTUBE_API_GATEWAY = 'https://www.googleapis.com/youtube/';
const REGEX_FORMAT_HEADER =
  /ID\s+EXT\s+RESOLUTION\s+FPS\s+\|\s+FILESIZE\s+TBR\s+PROTO\s+\|\s+VCODEC\s+VBR\s+ACODEC\s+ABR\s+ASR\s+MORE\s+INFO\s*\n(\-+\s*)+/i;

export interface LatestVideoData {
  items: {
    id: {videoId: string};
  }[];
}

export async function getLatestVideoIds(channelId: string): Promise<string[]> {
  try {
    let response = await Axios.get<LatestVideoData>(
      `${YOUTUBE_API_GATEWAY}v3/search`,
      {
        params: {
          key: YOUTUBE_API_KEY,
          channelId,
          maxResults: 10,
          order: 'date',
          type: 'video',
        },
      },
    );

    let data = response.data;

    return data.items.map(item => item.id.videoId);
  } catch (_error) {
    let error = _error as AxiosError;

    let errorData = error.response?.data?.error;

    if (errorData && 'code' in errorData && 'message' in errorData) {
      let {message} = errorData;

      throw new Error(message);
    }

    throw new Error(error.message);
  }
}

export interface VideoFormatInfo {
  width: number;
  height: number;
  definition: string;
  bitrate: number;
  codec: string;
  framerate: number;
}

export interface AudioFormatInfo {
  bitrate: number;
  codec: string;
  sampleRate: number;
}

export interface FormatInfo {
  code: number;
  extension: string;
  fileSize: number;
  video?: VideoFormatInfo;
  audio?: AudioFormatInfo;
}

export async function getAvailableFormats(
  videoId: string,
): Promise<FormatInfo[]> {
  let videoURL = getYouTubeVideoURL(videoId);

  return new Promise<FormatInfo[]>((resolve, reject) => {
    let childProcess = ChildProcess.exec(
      `${EXEC_PROGRAM} -F "${videoURL}"`,
      (error, stdout) => {
        if (error) {
          reject(error);
          return;
        }

        let match = stdout.match(REGEX_FORMAT_HEADER);

        if (!match) {
          reject(new Error('No format listed'));
          return;
        }

        let headerText = match[0];
        let headerIndex = match.index as number;

        let lineParser = generateLineSectionRangeMap(headerText);

        let formatLines = stdout
          .slice(headerIndex + headerText.length)
          .split('\n');

        let formats: FormatInfo[] = [];

        for (let formatLine of formatLines) {
          formatLine = formatLine.trim();
          let parsedMap = lineParser(formatLine);

          let format = getLineFormat(parsedMap);

          if (!format) {
            continue;
          }
          formats.push(format);
        }

        resolve(formats);
      },
    );

    childProcess.stderr?.pipe(process.stderr);
  });
}

export function generateLineSectionRangeMap(
  headerText: string,
): (line: string) => Map<string, string> {
  let lines = headerText.trim().split('\n');
  if (lines.length !== 2) {
    throw new Error('Header should be two lines');
  }
  let sectionSpaces = lines[1].split(' ');
  let startEnds: number[][] = [];
  let index = 0;
  for (let sectionSpace of sectionSpaces) {
    let start = index;
    let end = index + sectionSpace.length;
    startEnds.push([start, end]);
    index = end;
    index++;
  }
  let sectionRangeMap = new Map<string, number[]>();
  let headerLine = lines[0];
  for (let [start, end] of startEnds) {
    let title = headerLine.slice(start, end).trim();
    if (title === '|') {
      continue;
    }
    sectionRangeMap.set(title, [start, end]);
  }
  return (line: string): Map<string, string> => {
    let map = new Map<string, string>();
    for (let [key, [start, end]] of sectionRangeMap) {
      let value = line.slice(start, end).trim();
      if (value) {
        map.set(key, value);
      }
    }
    return map;
  };
}

function parseFileSize(size: string): number {
  size = size.trim();
  if (size.endsWith('MiB')) {
    return Number(size.slice(0, -3)) * 1024;
  } else if (size.endsWith('MB')) {
    return Number(size.slice(0, -2)) * 1000;
  } else if (size.endsWith('GiB')) {
    return Number(size.slice(0, -3)) * 1024 * 1024;
  } else if (size.endsWith('GB')) {
    return Number(size.slice(0, -2)) * 1000 * 1000;
  } else if (size.endsWith('B')) {
    return Number(size.slice(0, -1));
  }
  return Number(size);
}

function parseBitrate(bitrate: string): number {
  bitrate = bitrate.trim();
  if (bitrate.endsWith('k')) {
    return Number(bitrate.slice(0, -1)) * 1000;
  }
  return Number(bitrate);
}

function parseSampleRate(rate: string): number {
  rate = rate.trim();
  if (rate.endsWith('kHz')) {
    return Number(rate.slice(0, -3)) * 1000;
  } else if (rate.endsWith('Hz')) {
    return Number(rate.slice(0, -2));
  }
  return Number(rate);
}

export function getLineFormat(
  sectionMap: Map<string, string>,
): FormatInfo | undefined {
  if (
    !sectionMap.has('ID') ||
    !sectionMap.get('EXT') ||
    !sectionMap.get('FILESIZE')
  ) {
    return undefined;
  }
  let code = Number(sectionMap.get('ID'))!;
  let extension = sectionMap.get('EXT')!;
  let fileSize = parseFileSize(sectionMap.get('FILESIZE')!);
  let audioFormat: AudioFormatInfo | undefined;
  if (sectionMap.has('ACODEC')) {
    audioFormat = {
      codec: sectionMap.get('ACODEC')!,
      bitrate: parseBitrate(sectionMap.get('ABR')!),
      sampleRate: parseSampleRate(sectionMap.get('ASR')!),
    };
  }
  let videoFormat: VideoFormatInfo | undefined;
  if (sectionMap.has('VCODEC')) {
    let resolutionStr = sectionMap.get('RESOLUTION')!;
    let resolutions = resolutionStr.split('x');
    let moreInfo = sectionMap.get('MORE INFO')!;
    let colonIndex = moreInfo.indexOf(',');
    let definition = moreInfo;
    if (colonIndex !== -1) {
      definition = moreInfo.slice(0, colonIndex);
    }
    videoFormat = {
      codec: sectionMap.get('VCODEC')!,
      width: Number(resolutions[0]),
      height: Number(resolutions[1]),
      definition,
      bitrate: parseBitrate(sectionMap.get('VBR')!),
      framerate: Number(sectionMap.get('FPS')!),
    };
  }

  return {
    code,
    extension,
    fileSize,
    video: videoFormat,
    audio: audioFormat,
  };
}

export function selectBestDownloadFormat(
  formats: FormatInfo[],
  definition: string | number,
): DownloadFormat | undefined {
  definition = String(definition).toLowerCase();

  if (!definition.endsWith('p')) {
    definition += 'p';
  }

  let audioFormats = formats
    .filter(format => format.audio && !format.video)
    .sort((a, b) => b.fileSize - a.fileSize) as DownloadAudioOnlyFormat[];

  if (!audioFormats.length) {
    return undefined;
  }

  let audioOnly = audioFormats[0];

  if (audioFormats.length >= 2) {
    let m4aFormats = audioFormats
      .slice(0, 2)
      .filter(format => format.extension.toLowerCase() === 'm4a');
    if (m4aFormats.length) {
      audioOnly = m4aFormats[0];
    }
  }

  let videoFormats = formats
    .filter(
      format =>
        format.video &&
        !format.audio &&
        format.video.definition.toLowerCase() === definition,
    )
    .sort((a, b) => b.fileSize - a.fileSize) as DownloadVideoOnlyFormat[];

  if (!videoFormats.length) {
    return undefined;
  }

  let videoOnly = videoFormats[0];

  if (videoFormats.length >= 2) {
    let mp4Formats = videoFormats
      .slice(0, 2)
      .filter(format => format.extension.toLowerCase() === 'mp4');
    if (mp4Formats.length) {
      videoOnly = mp4Formats[0];
    }
  }

  return {
    videoOnly,
    audioOnly,
  };
}

export interface DownloadVideoOnlyFormat extends FormatInfo {
  audio: undefined;
  video: VideoFormatInfo;
}

export interface DownloadAudioOnlyFormat extends FormatInfo {
  audio: AudioFormatInfo;
  video: undefined;
}

export interface MergedDownloadFormatOption {
  videoOnly: DownloadVideoOnlyFormat;
  audioOnly: DownloadAudioOnlyFormat;
}

export type DownloadFormat = string | FormatInfo | MergedDownloadFormatOption;

export interface DownloadVideoOptions {
  toDir?: string;
  filenamePattern?: string;
  format?: DownloadFormat;
  writeDescription?: boolean;
  writeAutoSub?: boolean;
  subFormat?: string;
  writeThumbnail?: boolean;
  writeInfoJson?: boolean;
}

export interface VideoInfo {
  fulltitle: string;
  title: string;
  description: string;
}

export async function downloadVideo(
  videoId: string,
  options: DownloadVideoOptions = {},
) {
  let {
    toDir = process.execPath,
    format = 'best',
    filenamePattern = '%(title)s_%(id)s.%(ext)s',
    writeDescription,
    writeAutoSub,
    subFormat,
    writeThumbnail,
    writeInfoJson,
  } = options;

  let outputPath = toDir;

  let videoURL = getYouTubeVideoURL(videoId);

  let commands = [EXEC_PROGRAM];

  if (format) {
    if (typeof format === 'object') {
      if ('code' in format) {
        format = String(format.code);
      } else {
        let videoCode = format.videoOnly.code;
        let audioCode = format.audioOnly.code;
        format = `${videoCode}+${audioCode}`;
      }
    }

    commands.push(`-f "${format}"`);
  }
  if (filenamePattern) {
    outputPath = Path.join(outputPath, filenamePattern);
    commands.push(`-o "${outputPath}"`);
  }
  if (writeDescription) {
    commands.push('--write-description');
  }
  if (writeAutoSub) {
    commands.push('--write-auto-subs');
  }
  if (subFormat) {
    commands.push(`--sub-format ${subFormat}`);
  }
  if (writeThumbnail) {
    commands.push('--write-thumbnail');
  }
  if (writeInfoJson) {
    commands.push('--write-info-json');
  }
  commands.push(videoURL);

  let command = commands.join(' ');

  return new Promise<void>((resolve, reject) => {
    let childProcess = ChildProcess.exec(command, error => {
      if (error) {
        reject(error);
        return;
      }

      resolve();
    });

    childProcess.stdout?.pipe(process.stdout);
    childProcess.stderr?.pipe(process.stderr);
  });
}

export function getYouTubeVideoURL(videoId: string) {
  return `https://www.youtube.com/watch?v=${videoId}`;
}

export function getYouTubeChannelURL(channelId: string) {
  return `https://www.youtube.com/channel/${channelId}`;
}
